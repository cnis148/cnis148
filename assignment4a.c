#include<stdio.h>
void swap_pass_value(int a, int b)
{
      int temp;
      temp =  a;
      a =  b;
      b = temp;
      printf("The Value Of A And B After Swapping %d,%d\n",a,b);
}
void swap_pass_ref(int *a,int *b)
{
      int temp;
      temp =  *a;
      *a =  *b;
      *b = temp;
      printf("The Value Of A And B After Swapping %d,%d\n",*a,*b);
}
int main()
{
      int x,y;
      printf("Enter THe Numbers\n");
      scanf("%d%d",&x,&y);
      swap_pass_value(x,y);
      printf("After Calling swap in Main Function %d,%d\n",x,y);
      swap_pass_ref(&x,&y);
      printf("After Calling swap in Main Function %d,%d\n",x,y);
      return 0;
}