#include<stdio.h>
int power(int x, int y) 
{ 
	if (y == 0) 
		return 1; 
	else if (y%2 == 0) 
		return power(x, y/2)*power(x, y/2); 
	else
		return x*power(x, y/2)*power(x, y/2); 
} 

int main() 
{ 
	int x ; 
	int y ; 
	printf("Enter the x and y value\n");
	scanf("%d%d",&x,&y);
	printf("The value of x^n where x is %d and y is %d is %d ",x,y, power(x, y)); 
	return 0;  
} 