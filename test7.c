#include <stdio.h>
int main()
{
	char string[25], reverse_string[25] = {'\0'};
	int i, length = 0, value = 0;

	printf("Enter a string \n");
	scanf("%s",string) ;

	for (i = 0; string[i] != '\0'; i++)
	{
		length++;
	}
	printf("The length of the string '%s' = %d\n", string, length);
	for (i = length - 1; i >= 0 ; i--)
	{
		reverse_string[length - i - 1] = string[i];
	}

	for (value = 1, i = 0; i < length ; i++)
	{
		if (reverse_string[i] != string[i])
			value = 0;
	}
	if (value == 1)
		printf ("%s is a palindrome \n", string);
	else
		printf("%s is not a palindrome \n", string);

	return 0 ;
}