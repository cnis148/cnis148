#include<stdio.h>
int main()
{
    int marks[3][5];
    int i,j,max;
    for(i=0;i<3;i++)
    {
        printf("Subject %d\n",(i+1));
        for(j=0;j<5;j++)
        {
            printf("Enter the marks:\n");
            scanf("%d",&marks[i][j]);
        }
    }
    for(i=0;i<3;i++)
    {
        printf("Highest marks in subject %d:\n",(i+1));
        max=0;
        for(j=0;j<5;j++)
        {
            if(marks[i][j]>max)
            {
                max=marks[i][j];
            }
        }
        printf("%d\n",max);
    }
    return 0;
}